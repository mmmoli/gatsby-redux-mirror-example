import "babel-polyfill";
import React from 'react'
import { render } from 'mirrorx'

exports.replaceHydrateFunction = () => {
  return (element, container, callback) => {
    render(element, container, callback);
  };
};
