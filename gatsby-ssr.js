import React from "react";
import { options } from 'mirrorx/lib/defaults'
import { models } from 'mirrorx/lib/model'
import { Provider } from 'react-redux'
import { store, createStore } from 'mirrorx/lib/store'
import { renderToString } from "react-dom/server";


exports.replaceRenderer = ({ bodyComponent, replaceBodyHTMLString }) => {
  const { initialState, middlewares, reducers } = options

  createStore(models, reducers, initialState, middlewares)


  const ConnectedBody = () => (
    <Provider store={store}>{bodyComponent}</Provider>
  );

  replaceBodyHTMLString(renderToString(<ConnectedBody />));
};